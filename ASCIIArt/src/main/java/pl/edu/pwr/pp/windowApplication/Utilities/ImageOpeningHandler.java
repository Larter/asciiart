package pl.edu.pwr.pp.windowApplication.Utilities;

import com.sun.media.sound.InvalidFormatException;
import pl.edu.pwr.pp.PGMImageUtilities.ImageFileReader;
import pl.edu.pwr.pp.PGMImageUtilities.InvalidFileFormatException;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

public class ImageOpeningHandler {

    private ImageFileReader reader = new ImageFileReader();

    public BufferedImage getPictureFromFile(String filename) throws URISyntaxException, InvalidFileFormatException, IOException {
        if(filename.endsWith(".pgm"))
            return getPgmFile(filename);
        else
            return getNormalImageFile(filename);
    }

    private BufferedImage getNormalImageFile(String filename) throws IOException {
        return ImageIO.read(new File(filename));
    }

    private BufferedImage getPgmFile(String filename) throws URISyntaxException, InvalidFileFormatException, InvalidFormatException {
        return getBufferedImageFromPixelsOfPgm( reader.readPgmFile(Paths.get(filename)));
    }

    private BufferedImage getBufferedImageFromPixelsOfPgm(int [][] pixelsOfImageToSave) {
        BufferedImage image = new BufferedImage(pixelsOfImageToSave[0].length, pixelsOfImageToSave.length,
                BufferedImage.TYPE_BYTE_GRAY);
        WritableRaster raster = image.getRaster();
        for (int y = 0; y < pixelsOfImageToSave.length; y++) {
            for (int x = 0; (x < pixelsOfImageToSave[0].length); x++) {
                raster.setSample(x, y, 0, pixelsOfImageToSave[y][x]);
            }
        }
        return image;
    }

    public BufferedImage getPictureFromURL(String urlString) throws IOException, URISyntaxException, InvalidFileFormatException {
        URL url = new URL(urlString);
        if(urlString.endsWith(".pgm"))
            return getPgmFromURL(url);
        else
            return getNormalImageFromURL(url);
    }

    private BufferedImage getNormalImageFromURL(URL url) throws IOException {
        return ImageIO.read(url);
    }

    private BufferedImage getPgmFromURL(URL url) throws URISyntaxException, InvalidFileFormatException, InvalidFormatException {
        return getBufferedImageFromPixelsOfPgm(reader.readPgmFile(url));
    }



}
