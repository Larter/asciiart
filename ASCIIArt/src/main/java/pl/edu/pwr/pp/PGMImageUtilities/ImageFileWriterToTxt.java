package pl.edu.pwr.pp.PGMImageUtilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class ImageFileWriterToTxt implements  IImageFileWriter{

    public String saveToResults(char[][] ascii, String fileName) {
        String fullPath = prepareFolderAndGetFullPath(fileName);
        return saveToFile(ascii, fullPath);
    }

    public String saveToFile(char[][] ascii, String filePath) {
        File file = new File(filePath);

        PrintWriter printWriter = null;
        try {
            printWriter = new PrintWriter(file);
            for (char[] anAscii : ascii)
                printLine(printWriter, anAscii);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (printWriter != null)
                printWriter.close();
        }
        return file.getAbsolutePath();
    }

    private String prepareFolderAndGetFullPath(String fileName) {
        final String resultFolder = "results";
        new File(resultFolder).mkdir();
        return resultFolder + "/" + fileName;
    }

    private void printLine(PrintWriter printWriter, char[] anAscii) {
        String line = "";
        for (char f : anAscii) {
            line += f;
        }
        printWriter.println(line);
    }

}
