package pl.edu.pwr.pp.windowApplication.Utilities.ImageResizer;

import java.awt.*;
import java.awt.image.BufferedImage;

import static pl.edu.pwr.pp.windowApplication.Utilities.ImageResizer.Utilities.resizeImageToSize;

public class ImageResizerWindowSize implements IImageResizer
{
    @Override
    public String toString() {
        return "Szerokość okna";
    }

    @Override
    public BufferedImage resizeImage(BufferedImage image) {
        Dimension screenSize =
                Toolkit.getDefaultToolkit().getScreenSize();
        final int pixelsPerSign = 9;
        int w =(int) screenSize.getWidth()/ pixelsPerSign;
        int h =(image.getHeight()*w)/ image.getWidth();
        return resizeImageToSize(image, w, h);
    }
}
