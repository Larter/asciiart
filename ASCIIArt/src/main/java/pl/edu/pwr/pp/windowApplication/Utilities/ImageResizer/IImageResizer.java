package pl.edu.pwr.pp.windowApplication.Utilities.ImageResizer;

import java.awt.image.BufferedImage;

public interface IImageResizer {
    BufferedImage resizeImage(BufferedImage image);
}

