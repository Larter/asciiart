package pl.edu.pwr.pp.windowApplication.Utilities.ImageResizer;

import java.awt.image.BufferedImage;

import static pl.edu.pwr.pp.windowApplication.Utilities.ImageResizer.Utilities.resizeImageToSize;

public class ImageResizer160 implements IImageResizer
{
    @Override
    public String toString() {
        return "Szerokość 160 znaków";
    }

    @Override
    public BufferedImage resizeImage(BufferedImage image) {
        int w =160;
        int h =(image.getHeight()*w)/ image.getWidth();
        return resizeImageToSize(image, w, h);
    }
}
