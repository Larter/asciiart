package pl.edu.pwr.pp.windowApplication.GUI;

import pl.edu.pwr.pp.PGMImageUtilities.InvalidFileFormatException;
import pl.edu.pwr.pp.windowApplication.Utilities.ImageOpeningHandler;

import static javax.swing.JOptionPane.showMessageDialog;

import javax.swing.*;
import java.awt.event.*;
import java.io.IOException;
import java.net.URISyntaxException;

public class SelectFile extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JRadioButton zAdresuURLRadioButton;
    private JRadioButton zPlikuRadioButton;
    private JButton wczytajObrazButton;
    private JTextField zPlikuText;
    private JTextField zAdresuText;
    private MainWindow mainWindow;
    ImageOpeningHandler imageOpeningHandler = new ImageOpeningHandler();

    public SelectFile(MainWindow window) {
        mainWindow = window;

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(e -> onOK());
        buttonCancel.addActionListener(e -> onCancel());

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        zPlikuRadioButton.addActionListener(actionEvent -> {
            zAdresuURLRadioButton.setSelected(false);
            zPlikuText.setEnabled(true);
            zAdresuText.setEnabled(false);
        });
        zAdresuURLRadioButton.addActionListener(actionEvent -> {
            zPlikuRadioButton.setSelected(false);
            zPlikuText.setEnabled(false);
            zAdresuText.setEnabled(true);
        });

        zPlikuRadioButton.setSelected(true);
        zAdresuURLRadioButton.setSelected(false);
        zPlikuText.setEnabled(true);
        zAdresuText.setEnabled(false);

        wczytajObrazButton.addActionListener(actionEvent -> {
            JFileChooser chooser = new JFileChooser();
            chooser.setDialogTitle("Choose File");
            chooser.setAcceptAllFileFilterUsed(false);
            if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                zPlikuText.setText(chooser.getSelectedFile().getAbsolutePath());
            }
        });
    }

    private void onOK() {
        try {
            tryToDisplayPictureOnMainWindow();
            dispose();
        } catch (URISyntaxException e) {
            showMessageDialog(null, "Couldn't load file- invalid File URI");
        } catch (InvalidFileFormatException e) {
            showMessageDialog(null, "Couldn't load file- invalid PGM file");
        } catch (IOException e) {
            showMessageDialog(null, "Couldn't load file- not a picture");
        }
    }

    private void tryToDisplayPictureOnMainWindow() throws URISyntaxException, InvalidFileFormatException, IOException {
        if (zPlikuRadioButton.isSelected())
            mainWindow.displayPicture(imageOpeningHandler.getPictureFromFile(zPlikuText.getText()));
        if (zAdresuURLRadioButton.isSelected())
            mainWindow.displayPicture(imageOpeningHandler.getPictureFromURL(zAdresuText.getText()));
    }

    private void onCancel() {
        dispose();
    }

}
