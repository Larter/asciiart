package pl.edu.pwr.pp.windowApplication.Utilities.ImageResizer;

import java.awt.image.BufferedImage;

public class ImageResizerOriginal implements IImageResizer
{
    @Override
    public String toString() {
        return "Rozmiar oryginalny";
    }

    @Override
    public BufferedImage resizeImage(BufferedImage image) {
        return image;
    }
}
