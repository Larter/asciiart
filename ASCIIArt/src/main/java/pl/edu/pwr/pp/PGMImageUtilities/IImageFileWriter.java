package pl.edu.pwr.pp.PGMImageUtilities;

public interface IImageFileWriter {
    String saveToFile(char[][] ascii, String filePath);
}
