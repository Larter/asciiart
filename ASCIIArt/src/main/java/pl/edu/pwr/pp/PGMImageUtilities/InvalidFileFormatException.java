package pl.edu.pwr.pp.PGMImageUtilities;

public class InvalidFileFormatException extends Exception {
    public InvalidFileFormatException(String message) {
        super(message);
    }
}
