package pl.edu.pwr.pp.consoleApplication;

import pl.edu.pwr.pp.PGMImageUtilities.ImageConverter;
import pl.edu.pwr.pp.PGMImageUtilities.ImageFileReader;
import pl.edu.pwr.pp.PGMImageUtilities.ImageFileWriterToTxt;

public class AsciiArtApplication {
    static final String pgmExtension = ".pgm";
    static final String txtExtension = ".txt";

    public static void main(String[] args) {

        String[] images = new String[]{"Marilyn_Monroe", "Mona_Lisa", "Sierpinski_Triangle"};

        ImageFileReader imageFileReader = new ImageFileReader();
        ImageFileWriterToTxt imageFileWriter = new ImageFileWriterToTxt();

        for (String imageName : images) {
            try {
                int[][] intensities = imageFileReader.readPgmFileFromResources(imageName + pgmExtension);
                char[][] ascii = ImageConverter.intensitiesToAscii(intensities);
                imageFileWriter.saveToResults(ascii, imageName + txtExtension);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
