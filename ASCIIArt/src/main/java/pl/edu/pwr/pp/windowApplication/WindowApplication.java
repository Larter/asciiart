package pl.edu.pwr.pp.windowApplication;

import pl.edu.pwr.pp.windowApplication.GUI.MainWindow;

import javax.swing.*;

public class WindowApplication {
    public static void main(String[] args) {
        JFrame frame = new JFrame("MainWindow");
        frame.setContentPane(new MainWindow().MainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
