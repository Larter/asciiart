package pl.edu.pwr.pp.windowApplication.GUI;

import com.sun.media.sound.InvalidFormatException;
import pl.edu.pwr.pp.PGMImageUtilities.IImageFileWriter;
import pl.edu.pwr.pp.PGMImageUtilities.ImageFileWriterToPictureFile;
import pl.edu.pwr.pp.PGMImageUtilities.ImageFileWriterToTxt;
import pl.edu.pwr.pp.windowApplication.Utilities.ImageResizer.*;
import pl.edu.pwr.pp.windowApplication.Utilities.ImageSavingHandler;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public class MainWindow {
    public JPanel MainPanel;
    private JPanel imgPanel;
    private JLabel imgLabel;
    private JButton zapiszDoPlikuButton;
    private JButton opcja1Button;
    private JButton wczytajObrazButton;
    private JButton zapiszDoPngButton;
    private JButton funkcja2Button;
    private JComboBox<IImageResizer> sizeCombo;
    private JPanel realImage;
    private JLabel imageLabel;
    private SelectFile selectFileDialog = new SelectFile(this);
    private boolean useAsci70 = false;
    final String asci70String = "Asci 70";
    final String asci10String = "Asci 10";

    public MainWindow() {
        wczytajObrazButton.addActionListener(e -> displaySelectFileWindow());
        zapiszDoPlikuButton.addActionListener(e -> displaySaveToFileDialogForTxt());
        zapiszDoPngButton.addActionListener(e -> displaySaveToFileDialogForPicture());
        opcja1Button.addActionListener(actionEvent -> changeAsciButtonStatus());
        sizeCombo.addItem(new ImageResizerOriginal());
        sizeCombo.addItem(new ImageResizer80());
        sizeCombo.addItem(new ImageResizer160());
        sizeCombo.addItem(new ImageResizerWindowSize());
    }

    private void changeAsciButtonStatus() {
        if (useAsci70) {
            opcja1Button.setText(asci10String);
            useAsci70 = false;
        } else {
            opcja1Button.setText(asci70String);
            useAsci70 = true;
        }
    }

    private void displaySelectFileWindow() {
        selectFileDialog.revalidate();
        selectFileDialog.pack();
        selectFileDialog.setVisible(true);
    }

    private void displaySaveToFileDialogForTxt() {
        displaySaveToFileDialog(new ImageFileWriterToTxt());
    }

    private void displaySaveToFileDialogForPicture() {
        displaySaveToFileDialog(new ImageFileWriterToPictureFile());
    }

    private void displaySaveToFileDialog(IImageFileWriter writer) {
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("Choose File");
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            final String pathToSave = chooser.getSelectedFile().getAbsolutePath();
            saveImageAsAsciArt(pathToSave,writer);
        }
    }

    private void saveImageAsAsciArt(String pathToSave, IImageFileWriter writer) {
        BufferedImage imageToSave = ((ImagePanel) imgPanel).getImage();
        final IImageResizer imageResizer = (IImageResizer) sizeCombo.getSelectedItem();
        ImageSavingHandler imageSavingHandler = new ImageSavingHandler(writer);
        imageSavingHandler.saveImage(pathToSave, imageToSave, imageResizer, useAsci70);
    }

    private void createUIComponents() {
        imgPanel = new ImagePanel();
        imgPanel.setPreferredSize(new Dimension(300, 300));
        opcja1Button = new JButton();
        useAsci70 = false;
        opcja1Button.setEnabled(true);
        sizeCombo = new JComboBox<>();
    }


    public void displayPicture(BufferedImage myPicture) throws InvalidFormatException {
        if (myPicture == null) {
            throw new InvalidFormatException("This is not a picture");
        }
        ((ImagePanel) imgPanel).setBackground(myPicture);
        zapiszDoPlikuButton.setEnabled(true);
        zapiszDoPngButton.setEnabled(true);
        imgPanel.revalidate();
        imgPanel.repaint();
    }
}

