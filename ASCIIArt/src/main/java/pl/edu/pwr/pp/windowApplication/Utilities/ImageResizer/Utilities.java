package pl.edu.pwr.pp.windowApplication.Utilities.ImageResizer;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Utilities {
    public static BufferedImage resizeImageToSize(BufferedImage image, int w, int h) {
        BufferedImage resizedImage = new BufferedImage(w, h, image.getType());
        Graphics2D g = resizedImage.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.drawImage(image, 0, 0, w, h, null);
        g.dispose();
        return resizedImage;
    }
}
