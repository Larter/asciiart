package pl.edu.pwr.pp.PGMImageUtilities;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.stream.IntStream;

public class ImageFileWriterToPictureFile implements IImageFileWriter {

    final int widthMultiplayer = 22;
    final int heightMultiplayer = 27;
    final Font font = new Font("Courier New", Font.BOLD, 25);

    @Override
    public String saveToFile(char[][] ascii, String filePath) {
        File file = new File(filePath);
        BufferedImage image = new BufferedImage(ascii[0].length * widthMultiplayer, ascii.length * heightMultiplayer, BufferedImage.TYPE_INT_RGB);
        drawAsci(ascii, widthMultiplayer, heightMultiplayer, image);
        writeImageToFile(file, image);
        return file.getAbsolutePath();
    }

    private void drawAsci(char[][] ascii, int widthMultiplayer, int heightMultiplayer, BufferedImage image) {
        Graphics graphics = image.createGraphics();
        graphics.setColor(Color.white);
        graphics.fillRect(0, 0, image.getWidth(), image.getHeight());
        fillGraphicsWithAsci(ascii, widthMultiplayer, heightMultiplayer, graphics);
    }

    private void fillGraphicsWithAsci(char[][] ascii, int widthMultiplayer, int heightMultiplayer, Graphics graphics) {
        graphics.setColor(Color.black);
        graphics.setFont(font);
        IntStream.range(0, ascii.length).forEach(
                i -> IntStream.range(0, ascii[0].length).forEach(j -> graphics.drawString("" + ascii[i][j], j * widthMultiplayer, i * heightMultiplayer)
                )
        );
    }

    private void writeImageToFile(File file, BufferedImage image) {
        try {
            ImageIO.write(image, "png", file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
