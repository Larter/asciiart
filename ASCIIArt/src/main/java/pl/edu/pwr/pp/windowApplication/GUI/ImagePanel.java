package pl.edu.pwr.pp.windowApplication.GUI;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.nio.Buffer;
import javax.swing.JPanel;

public class ImagePanel extends JPanel {

    public BufferedImage getImage() {
        return image;
    }

    BufferedImage image;

    public void setBackground(BufferedImage image) {
        this.image = image;
    }

    @Override
    public void paintComponent(Graphics G) {
        super.paintComponent(G);
        G.drawImage(image, 0, 0, this.getWidth(), this.getHeight(), null);
    }
}