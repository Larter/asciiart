package pl.edu.pwr.pp.windowApplication.Utilities;

import pl.edu.pwr.pp.PGMImageUtilities.IImageFileWriter;
import pl.edu.pwr.pp.PGMImageUtilities.ImageConverter;
import pl.edu.pwr.pp.PGMImageUtilities.ImageFileWriterToTxt;
import pl.edu.pwr.pp.windowApplication.Utilities.ImageResizer.IImageResizer;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

public class ImageSavingHandler {
    public ImageSavingHandler(IImageFileWriter imgWriter) {writer=imgWriter;}
    public ImageSavingHandler() {
        writer = new ImageFileWriterToTxt();
    }

    IImageFileWriter writer;

    public void saveImage(String pathToSave, BufferedImage imageToSave, IImageResizer imageResizer, boolean useAsci70) {
        imageToSave = imageResizer.resizeImage(imageToSave);
        writer.saveToFile(getAsciArray(imageToSave, useAsci70), pathToSave);
    }

    private char[][] getAsciArray(BufferedImage imageToSave, boolean useAsci70) {
        return ImageConverter.intensitiesToAscii(getImageIntensities(imageToSave), useAsci70);
    }

    private int[][] getImageIntensities(BufferedImage imageToSave) {
        int[][] intensitiesOfImageToSave;

        if (imageToSave.getType() == BufferedImage.TYPE_BYTE_GRAY)
            intensitiesOfImageToSave = getIntensitiesGrayscale(imageToSave);
        else
            intensitiesOfImageToSave = getIntensitiesClored(imageToSave);
        return intensitiesOfImageToSave;
    }

    private int[][] getIntensitiesClored(BufferedImage imageToSave) {
        int[][] pixelsOfImageToSave;
        pixelsOfImageToSave = new int[imageToSave.getHeight()][imageToSave.getWidth()];
        for (int y = 0; y < pixelsOfImageToSave.length; y++) {
            for (int x = 0; (x < pixelsOfImageToSave[0].length); x++) {
                Color color = new Color(imageToSave.getRGB(x, y));
                pixelsOfImageToSave[y][x] = 255 - (int) (0.2989 * color.getRed() + 0.5870 * color.getGreen() + 0.1140 * color.getBlue());
            }
        }
        return pixelsOfImageToSave;
    }

    private int[][] getIntensitiesGrayscale(BufferedImage imageToSave) {
        int[][] pixelsOfImageToSave;
        pixelsOfImageToSave = new int[imageToSave.getHeight()][imageToSave.getWidth()];
        WritableRaster raster = imageToSave.getRaster();
        for (int y = 0; y < pixelsOfImageToSave.length; y++) {
            for (int x = 0; (x < pixelsOfImageToSave[0].length); x++) {
                pixelsOfImageToSave[y][x] = raster.getSample(x, y, 0);
            }
        }
        return pixelsOfImageToSave;
    }
}
