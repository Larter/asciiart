package pl.edu.pwr.pp.PGMImageUtilities;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.stream.IntStream;

public class ImageConverter {

    /**
     * Znaki odpowiadające kolejnym poziomom odcieni szarości - od czarnego (0)
     * do białego (255).
     */
    public static final String INTENSITY_2_ASCII = "@%#*+=-:. ";
    public static final String INTENSITY_2_ASCII70 = "$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/|()1{}[]?-_+~<>i!lI;:,\"^`'. ";

    /**
     * Metoda zwraca znak odpowiadający danemu odcieniowi szarości. Odcienie
     * szarości mogą przyjmować wartości z zakresu [0,255]. Zakres jest dzielony
     * na równe przedziały, liczba przedziałów jest równa ilości znaków w
     * {@value #INTENSITY_2_ASCII}. Zwracany znak jest znakiem dla przedziału,
     * do którego należy zadany odcień szarości.
     *
     * @param intensity odcień szarości w zakresie od 0 do 255
     * @return znak odpowiadający zadanemu odcieniowi szarości
     */
    public static char intensityToAscii(int intensity) {
        return intensityToAscii(intensity, false);
    }

    public static char intensityToAscii(int intensity, boolean useAsci70) {
        if (useAsci70)
            return INTENSITY_2_ASCII70.charAt((intensity * INTENSITY_2_ASCII70.length()) / 256);
        else
            return INTENSITY_2_ASCII.charAt((intensity * INTENSITY_2_ASCII.length()) / 256);
    }

    /**
     * Metoda zwraca dwuwymiarową tablicę znaków ASCII mając dwuwymiarową
     * tablicę odcieni szarości. Metoda iteruje po elementach tablicy odcieni
     * szarości i dla każdego elementu wywołuje {@ref #intensityToAscii(int)}.
     *
     * @param intensities tablica odcieni szarości obrazu
     * @return tablica znaków ASCII
     */

    public static char[][] intensitiesToAscii(int[][] intensities, boolean useAsci70) {

        char values[][] = new char[intensities.length][];
        IntStream.range(0, intensities.length).forEach(
                i -> {
                    values[i] = new char[intensities[i].length];
                    IntStream.range(0, intensities[i].length).forEach(
                            j -> values[i][j] = intensityToAscii(intensities[i][j], useAsci70)
                    );
                });

        return values;
    }

    public static char[][] intensitiesToAscii(int[][] intensities) {
        return intensitiesToAscii(intensities, false);
    }
}
