package pl.edu.pwr.pp.PGMImageUtilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ImageFileReader {

    private class Size {
        public int rows;
        public int columns;
    }

    /**
     * Metoda czyta plik pgm i zwraca tablicę odcieni szarości.
     *
     * @param fileName nazwa pliku pgm
     * @return tablica odcieni szarości odczytanych z pliku
     * @throws URISyntaxException jeżeli plik nie istnieje
     */
    public int[][] readPgmFileFromResources(String fileName) throws URISyntaxException, IOException, InvalidFileFormatException {
        Path path = this.getPathToResourcesFile(fileName);
        return readPgmFile(path);
    }

    public int[][] readPgmFile(Path path) throws URISyntaxException, InvalidFileFormatException {
        try (BufferedReader reader = Files.newBufferedReader(path)) {
            return readIntensities(reader);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public int[][] readPgmFile(URL url) throws URISyntaxException, InvalidFileFormatException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()))) {
            return readIntensities(reader);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private int[][] readIntensities(BufferedReader reader) throws IOException, URISyntaxException, InvalidFileFormatException {
        Size size = readHeader(reader);
        return readIntensities(size.columns, size.rows, reader);
    }

    private Size readHeader(BufferedReader reader) throws IOException, URISyntaxException, InvalidFileFormatException {
        readPNGHeader(reader);
        Size size = getSize(reader);
        readMaxBrightnessValue(reader);
        return size;
    }

    private Size getSize(BufferedReader reader) throws IOException, InvalidFileFormatException {
        String comment = reader.readLine().trim();
        String sizeString = null;
        if (comment.charAt(0) == '#')
            sizeString = reader.readLine();
        else
            sizeString = comment;
        return getSizeFromSizeString(sizeString);
    }

    private Size getSizeFromSizeString(String sizeString) throws InvalidFileFormatException {
        String[] sizeStringArray = sizeString.split(" ");
        Size size = new Size();
        try {
            size.rows = Integer.parseInt(sizeStringArray[1]);
            size.columns = Integer.parseInt(sizeStringArray[0]);
        } catch (NullPointerException e) {
            throw new InvalidFileFormatException("Size line is incorrect");
        }
        if (size.rows <= 0 || size.columns <= 0)
            throw new InvalidFileFormatException("Size value is negative or 0");
        return size;
    }

    private int[][] readIntensities(int columns, int rows, BufferedReader reader) throws IOException, InvalidFileFormatException {
        int[][] intensities = getIntensitiesEmptyArray(columns, rows);
        String line = null;
        int currentRow = 0;
        int currentColumn = 0;
        while ((line = reader.readLine()) != null) {
            String[] elements = line.split(" ");
            for (String element : elements) {
                intensities[currentRow][currentColumn] = Integer.parseInt(element);
                currentColumn++;
                if (currentColumn == columns) {
                    currentRow++;
                    currentColumn = 0;
                }
            }
        }
        checkIfAllRowsAreCovered(rows, currentRow, currentColumn);

        return intensities;
    }

    private int[][] getIntensitiesEmptyArray(int columns, int rows) {
        int[][] intensities;
        intensities = new int[rows][];
        for (int i = 0; i < rows; i++) {
            intensities[i] = new int[columns];
        }
        return intensities;
    }

    private void checkIfAllRowsAreCovered(int rows, int currentRow, int currentColumn) throws InvalidFileFormatException {
        if (currentRow != rows || currentColumn != 0)
            throw new InvalidFileFormatException("Values in image are missing");
    }

    private void readMaxBrightnessValue(BufferedReader reader) throws InvalidFileFormatException, IOException {
        String stringMax = reader.readLine().trim();
        if (!stringMax.equals("255")) {
            throw new InvalidFileFormatException("Wrong intensity value :" + stringMax + " - should be 255");
        }
    }

    private void readPNGHeader(BufferedReader reader) throws InvalidFileFormatException, IOException {
        String P2 = reader.readLine().trim();
        if (!P2.equals("P2"))
            throw new InvalidFileFormatException("Missing P2 in the header");
    }

    private Path getPathToResourcesFile(String fileName) throws URISyntaxException {
        try {
            URI uri = ClassLoader.getSystemResource(fileName).toURI();
            return Paths.get(uri);
        } catch (Exception e) {
            throw new URISyntaxException("error", "error");
        }

    }

}
