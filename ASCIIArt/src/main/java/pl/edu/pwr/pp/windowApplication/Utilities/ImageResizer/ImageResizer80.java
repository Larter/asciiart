package pl.edu.pwr.pp.windowApplication.Utilities.ImageResizer;

import java.awt.image.BufferedImage;

import static pl.edu.pwr.pp.windowApplication.Utilities.ImageResizer.Utilities.resizeImageToSize;

public class ImageResizer80 implements IImageResizer
{
    @Override
    public String toString() {
        return "Szerokość 80 znaków";
    }

    @Override
    public BufferedImage resizeImage(BufferedImage image) {
        int w =80;
        int h =(image.getHeight()*w)/ image.getWidth();
        return resizeImageToSize(image, w, h);
    }
}
