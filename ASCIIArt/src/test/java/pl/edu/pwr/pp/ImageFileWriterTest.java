package pl.edu.pwr.pp;

import org.junit.Assert;
import org.junit.Test;
import pl.edu.pwr.pp.PGMImageUtilities.ImageFileWriterToTxt;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class ImageFileWriterTest {
    ImageFileWriterToTxt writer = new ImageFileWriterToTxt();

    @Test
    public void shallProperlyWriteFile() {
        char[][] testArray = {{'%', ' '}, {'#', '$'}};
        final String testFilename = "filename";

        String filename = writer.saveToResults(testArray, testFilename);

        assertThat(filename, endsWith("/results/" + testFilename));
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(filename))) {
            for (char[] row : testArray)
                assertThat(reader.readLine(), is(equalTo(new String(row))));
        } catch (Exception e) {
            Assert.fail("Should not throw");
        }
        try {
            Files.delete(Paths.get(filename));
        } catch (IOException e) {
            Assert.fail("File couldn't be deleted");
        }
    }
}
