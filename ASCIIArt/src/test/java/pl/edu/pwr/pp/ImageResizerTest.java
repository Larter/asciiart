package pl.edu.pwr.pp;

import org.junit.Assert;
import org.junit.Test;
import pl.edu.pwr.pp.windowApplication.Utilities.ImageResizer.*;

import java.awt.*;
import java.awt.image.BufferedImage;

import static pl.edu.pwr.pp.windowApplication.Utilities.ImageResizer.Utilities.resizeImageToSize;

public class ImageResizerTest {
    @Test
    public void resizeImageToSizeTest() {
        BufferedImage image = new BufferedImage(100, 100, BufferedImage.TYPE_BYTE_GRAY);
        final int expectedWidth = 50;
        final int expectedHeight = 90;
        BufferedImage result =  resizeImageToSize(image, expectedWidth, expectedHeight);

        Assert.assertEquals(expectedWidth,result.getWidth());
        Assert.assertEquals(expectedHeight,result.getHeight());
    }

    @Test
    public void imageResizer80Test()
    {
        final int originalWidth = 250;
        final int originalHeight = 120;
        final int expectedWidth = 80;
        final int expectedHeight = (originalHeight*expectedWidth) / originalWidth;

        BufferedImage image = new BufferedImage(originalWidth, originalHeight, BufferedImage.TYPE_BYTE_GRAY);

        IImageResizer resizer= new ImageResizer80();
        BufferedImage result = resizer.resizeImage(image);

        Assert.assertEquals(expectedWidth,result.getWidth());
        Assert.assertEquals(expectedHeight,result.getHeight());
        Assert.assertEquals(resizer.toString(), "Szerokość 80 znaków");
    }

    @Test
    public void imageResizer160Test()
    {
        final int originalWidth = 250;
        final int originalHeight = 120;
        final int expectedWidth = 160;
        final int expectedHeight = (originalHeight*expectedWidth) / originalWidth;

        BufferedImage image = new BufferedImage(originalWidth, originalHeight, BufferedImage.TYPE_BYTE_GRAY);

        IImageResizer resizer= new ImageResizer160();
        BufferedImage result = resizer.resizeImage(image);

        Assert.assertEquals(expectedWidth,result.getWidth());
        Assert.assertEquals(expectedHeight,result.getHeight());
        Assert.assertEquals(resizer.toString(), "Szerokość 160 znaków");
    }

    @Test
    public void imageResizerOriginalTest()
    {
        final int originalWidth = 250;
        final int originalHeight = 120;

        BufferedImage image = new BufferedImage(originalWidth, originalHeight, BufferedImage.TYPE_BYTE_GRAY);


        IImageResizer resizer= new ImageResizerOriginal();
        BufferedImage result = resizer.resizeImage(image);

        Assert.assertEquals(originalWidth,result.getWidth());
        Assert.assertEquals(originalHeight,result.getHeight());
        Assert.assertEquals(resizer.toString(), "Rozmiar oryginalny");
    }

    @Test
    public void imageResizerWindowSizeTest()
    {
        final int originalWidth = 250;
        final int originalHeight = 120;

        Dimension screenSize =
                Toolkit.getDefaultToolkit().getScreenSize();
        final int pixelsPerSign = 9;

        final int expectedWidth = (int) screenSize.getWidth()/ pixelsPerSign;
        final int expectedHeight = (originalHeight*expectedWidth)/ originalWidth;


        BufferedImage image = new BufferedImage(originalWidth, originalHeight, BufferedImage.TYPE_BYTE_GRAY);

        IImageResizer resizer= new ImageResizerWindowSize();
        BufferedImage result = resizer.resizeImage(image);

        Assert.assertEquals(expectedWidth,result.getWidth());
        Assert.assertEquals(expectedHeight,result.getHeight());
        Assert.assertEquals(resizer.toString(), "Szerokość okna");
    }
}
