package pl.edu.pwr.pp;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static pl.edu.pwr.pp.PGMImageUtilities.ImageConverter.intensitiesToAscii;
import static pl.edu.pwr.pp.PGMImageUtilities.ImageConverter.intensityToAscii;

public class ImageConverterArrayTest {
    @Test
    public void shouldReturnRightArrayForIntensities() {
        int[][] intensities = {{1, 230}, {69, 33}};
        char[][] intensitiesAsci = intensitiesToAscii(intensities);
        for (int i = 0; i < intensities.length; i++) {
            for (int j = 0; j < intensities[i].length; j++) {
                assertThat(intensitiesAsci[i][j], is(equalTo(intensityToAscii(intensities[i][j]))));
            }
        }
    }
}
