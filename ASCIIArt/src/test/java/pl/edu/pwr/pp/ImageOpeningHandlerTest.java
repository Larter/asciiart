package pl.edu.pwr.pp;

import org.junit.Assert;
import org.junit.Test;
import pl.edu.pwr.pp.PGMImageUtilities.InvalidFileFormatException;
import pl.edu.pwr.pp.windowApplication.Utilities.ImageOpeningHandler;

import java.io.IOException;
import java.net.URISyntaxException;

public class ImageOpeningHandlerTest {

    ImageOpeningHandler handler = new ImageOpeningHandler();

    @Test
    public void shallThrowWithInvalidPathFromFile()
    {
        try {
            handler.getPictureFromFile("");
            Assert.fail();
        } catch (Exception ignored) {
        }

    }

    @Test
    public void shallThrowWithInvalidPathFromFilePGM()
    {
        try {
            handler.getPictureFromFile("nonexist.pgm");
            Assert.fail();
        } catch (Exception ignored) {
        }

    }


    @Test
    public void shallThrowWithInvalidPathFromUrl()
    {
        try {
            handler.getPictureFromURL("");
            Assert.fail();
        } catch (Exception ignored) {
        }

    }

    @Test
    public void shallThrowWithInvalidPathFromURLPGM()
    {
        try {
            handler.getPictureFromURL("nonexist.pgm");
            Assert.fail();
        } catch (Exception ignored) {
        }

    }

}
