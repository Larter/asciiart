package pl.edu.pwr.pp;

import org.junit.Assert;
import org.junit.Test;
import pl.edu.pwr.pp.PGMImageUtilities.ImageFileWriterToTxt;
import pl.edu.pwr.pp.windowApplication.Utilities.ImageResizer.IImageResizer;
import pl.edu.pwr.pp.windowApplication.Utilities.ImageSavingHandler;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

import static pl.edu.pwr.pp.PGMImageUtilities.ImageConverter.intensitiesToAscii;

public class ImageSavingHandlerTest {


    class ImageResizerStub implements IImageResizer {
        public BufferedImage resizeImage(BufferedImage image)
        {
            return  image;
        };
    }

    int pixels_pgm[][] = {{0, 255}, {90, 100}};

    class ImageFileWriterMock extends ImageFileWriterToTxt {

        boolean useAsci70_;
        ImageFileWriterMock(boolean useAsci70)
        {
            useAsci70_=useAsci70;
        }

        @Override
        public String saveToFile(char[][] ascii, String filePath) {
            char[][] asci_pixels = intensitiesToAscii(pixels_pgm,useAsci70_);
            Assert.assertEquals(ascii[0][0],asci_pixels[0][0]);
            Assert.assertEquals(ascii[0][1],asci_pixels[0][1]);
            Assert.assertEquals(ascii[1][0],asci_pixels[1][0]);
            Assert.assertEquals(ascii[1][1],asci_pixels[1][1]);
            return filePath;
        }
    }


    @Test
    public void imageSavingHandlerSaveFileTest_Greyscale_ASCI10()
    {
        final String filename = "file.txt";

        ImageSavingHandler handler = new ImageSavingHandler(new ImageFileWriterMock(false));

        BufferedImage image = new BufferedImage(2, 2, BufferedImage.TYPE_BYTE_GRAY);
        WritableRaster raster = image.getRaster();

        raster.setSample(0, 0, 0, pixels_pgm[0][0]);
        raster.setSample(1, 0, 0, pixels_pgm[0][1]);
        raster.setSample(0, 1, 0, pixels_pgm[1][0]);
        raster.setSample(1, 1, 0, pixels_pgm[1][1]);

        handler.saveImage(filename,image, new ImageResizerStub(), false);
    }
}
