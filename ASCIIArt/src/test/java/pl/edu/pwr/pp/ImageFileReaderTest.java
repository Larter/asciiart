package pl.edu.pwr.pp;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import java.net.URISyntaxException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import pl.edu.pwr.pp.PGMImageUtilities.ImageFileReader;
import pl.edu.pwr.pp.PGMImageUtilities.InvalidFileFormatException;

public class ImageFileReaderTest {

    ImageFileReader imageReader;

    @Before
    public void setUp() {
        imageReader = new ImageFileReader();
    }

    @Test
    public void shouldReadSequenceFrom0To255GivenTestImage() {
        // given
        String fileName = "testImage.pgm";
        // when
        int[][] intensities = null;
        try {
            intensities = imageReader.readPgmFileFromResources(fileName);
        } catch (Exception e) {
            Assert.fail("Should read the file");
        }
        // then
        int counter = 0;
        for (int[] row : intensities) {
            for (int intensity : row) {
                assertThat(intensity, is(equalTo(counter++)));
            }
        }
    }

    @Test
    public void shouldThrowExceptionWhenFileDoesntExist() {
        // given
        String fileName = "nonexistent.pgm";
        try {
            // when
            imageReader.readPgmFileFromResources(fileName);
            // then
            Assert.fail("Should throw exception");
        } catch (Exception e) {
            if (!(e instanceof URISyntaxException))
                Assert.fail("Should throw InvalidFileFormatException");
        }
    }

    @Test
    public void shouldThrowExceptionWhenFileHasWrongHeader() {
        final String testFile = "fileWithWrongHeader.pgm";
        expectFileThrowInvalidFileFormatException(testFile);
    }

    private void expectFileThrowInvalidFileFormatException(String testFile) {
        try {
            imageReader.readPgmFileFromResources(testFile);
            Assert.fail("Should throw exception");
        } catch (Exception e) {
            if (!(e instanceof InvalidFileFormatException))
                Assert.fail("Should throw InvalidFileFormatException");
        }
    }

    @Test
    public void shouldThrowExceptionWhenFileHasInvalidSizeLine() {
        expectFileThrowInvalidFileFormatException("fileWithWrongSizeLine.pgm");
    }

    @Test
    public void shouldThrowExceptionWhenMaxBrightnessIsNot255() {
        expectFileThrowInvalidFileFormatException("fileWithInvalidMaxBrightness.pgm");
    }

    @Test
    public void shouldThrowExceptionWhenNumberOfValuesIsWrong() {
        expectFileThrowInvalidFileFormatException("fileWithInvalidCountOfValues.pgm");
    }
}
